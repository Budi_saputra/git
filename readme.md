# GIT Course
==================================

## What is GIT

Version Control System (VCS) for tracking changes in computer file

- [x]  Distributed version control 
- [x]  Coordinates work between multiple developers
- [x]  Who made what changes and when
- [x]  Revert back at any time
- [x]  Local & Remote repos

## Concept Of Git

- [x]  Keeps track of code history
- [x]  Takes "Snapshots" of your file
- [x]  You decide when to take a snapshot by making a "Commit"
- [x]  You can visit any shapshots at any time

## Installing GIT

- [x] Linux (Ubuntu / Debian , dll)

    $ sudo apt-get install git
https://git-scm.com/download/linux

- [x] Linux (Fedora)

    $ sudo yum install git
    https://git-scm.com/download/linux

- [x] Mac

https://git-scm.com/download/mac

- [x] Windows

https://git-scm.com/download/win


## Basic Command

### Git Version

    $ git --version

### Check Information

    $ git config --global --list

### Initialize Local Git Repository

    $ git init

### Add File(s) To index

    $ git add <file or .>

### Check Status Of Working Tree

    $ git status

### Commit Changes in index or file

    $ git commit -m ""

### Log Status Commit

    $ git log or git log --oneline

### status git not save add and commit

    $ git diff

### Undo and Move Log
    /* Undo */ 
    $ git checkout file(index.html, dll) // not save add and commit

    /* Move Log */
    $ git checkout Commit_Number -- file(index.html, dll)

### Reset or Delete Log commit Working directory 

    $ git reset -- mixed Number_Commit // Delete Log Commit but not delete Working Directory

    $ git reset -- hard Number_Commit // Delete all Log and working directory

### Branch Repository

    $ git branch // Status Branch
    $ git branch Name_Branch // new Branch
    $ git checkout -b name_Branch // Outomatic switch New Branch
    $ git branch -D Name_Branch // Delete Branch
    $ git branch -m Name_Branch New_Branch // Change name Branch

### Push To Remote Repository

    $ git push

### Clone repository Into A New Directory

    $ git clone 

### status information branch repository

    $ git branch -a

### Pull Latest From Remote Repository

    $ git pull 

### Conflict

https://youtu.be/K9TCapFhenM

### Full basic Command 

https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html


## SSH KEY 

# Gitlab Course
====================================

## Repository Master Gitlab
* Make New Project Master 
* Project Name WebApp
* Make Project Gitlab
* git remote master 
* git push -u origin master

## Team Develop Project Frond End
* Team A make Web Guest So Branch WebGuest
Team Develop WeGuest : Budi Saputra and Hamdani 
* Team B Make Web Customer So Branch Customer
Team Develop Customer : Abdul Muiz and Yazid
* Team C Make Web Admin So Brach Admin
Team Develop Admin : Ari and Agung

### WebGuest

/* Budi */
* git clone Master gitlab/github in Directory Local
* make Branch WebGuest
* git checkout WebGuest
* Working directory
* git add . or file
* git commit -m " "
* git push -u origin WebGuest

/* Hamdani */
* git clone Master gitlab/github in Directory Local
* git branch -a // Status branch
* git checkout WebGuest
* git pull // to look change 
* working
* git add . or file
* git commit -m " "
* git push -u origin Webguest 

Note : jika saling push file WebGuest ke gitlab ,, pasti akan terjadi conflict ,, jika ingin push lebih baik git pull terlebih dahulu , untuk update file yang sudah di ubah di gitlab/github . Jika terjadi conflict ubah langsung dari directory kerja Source code .

/* master */
* Finish Working Webguest 
* Merge WebGuest to master in Gitlab / Github 


















